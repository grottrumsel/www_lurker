WWW Lurker
=======================


Overview
--------

This package provides "WWW Lurker" a search service, wrote as a recruitment task.

Installation
------------

Install using pip  ...


    $ pip install -r requirements.txt

Setup
------------

Starting celery


    $ celery -A www_lurker worker -l info

Starting django app (for non-production testing).


    $ python manage.py runserver
	
With assumption of default "127.0.0.1:8000" setting, type in your browser:


    http://127.0.0.1:8000/static/client/index.htm

Usage
------------

On a "Register/Login" tab you should see a form. 
Use all of the form to register, and then, two top fields to login.
Checkout your webdeveloper console for messages of a success or failure.
Once you're logged in, go to "Searches" tab.
Use "resources_list.txt" from this package to test. After using "Search!" button,
start refreshing list of tasks with "Refresh" button. Remember about messages in
console.

Misc
------------

Insert something like

    import time;time.sleep(10);

in lurker/task.py in task to give yourself a chance of seeing "STARTED" status.

There is one resource in "resources_list.txt" that causes exception while processing. 
This is for a purpouse of seeing "FAILURE" status after refreshing.

by Paweł B. 2017