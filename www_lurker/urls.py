# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from www_lurker.lurker.views import ProcessingSessionAPIView, TaskToSessionAPIView
from www_lurker.lurker_auth.views import UserRegistrationAPIView

urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^registration/', UserRegistrationAPIView.as_view()),
    url(r'^session/$', ProcessingSessionAPIView.as_view()),
    url(r'^tasks/', TaskToSessionAPIView.as_view(), name='tasks-list'),
]
