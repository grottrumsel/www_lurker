# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import exceptions
import urllib2

from celery import shared_task


REQUEST_HEADERS = {
    'Accept': 'text/html',
    'Accept-Charset': 'ISO-8859-1, UTF-8;q=0.8',
    # niektorzy nie lubią jak ich odwiedzać nie z przegladarki
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0'
}


def get_resource_content_from_url(resource_url):
    request = urllib2.Request(resource_url, headers=REQUEST_HEADERS)
    response = urllib2.urlopen(request)
    return response.read()

@shared_task
def process_resource(resource_url, keywords):
    content = get_resource_content_from_url(resource_url)
    if content:
        keywords_in_content = []
        keywords_not_in_content = []

        for one_keyword in keywords:
            keyword_found = False
            try:
                keyword_found = one_keyword in content
            except exceptions.UnicodeDecodeError:
                content = content.decode('utf8')
                keyword_found = one_keyword in content
            if keyword_found:
                keywords_in_content.append(one_keyword)
            else:
                keywords_not_in_content.append(one_keyword)
        return {
            'in_content': keywords_in_content,
            'not_in_content': keywords_not_in_content,
        }
    else:
        raise Exception("Wrong resource url.")
