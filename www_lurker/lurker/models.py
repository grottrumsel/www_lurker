# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re

from ast import literal_eval

from celery.states import (
    PENDING,
    SUCCESS,
)

from django_celery_results.models import TaskResult
from django.conf import settings
from django.db import models

from .tasks import process_resource


class TaskToSession(models.Model):

    def __init__(self, *args, **kwargs):
        if 'keywords' in kwargs:
            # nie ma potrzeby zapisywania ich w db
            self.keywords = kwargs.pop('keywords')
        super(TaskToSession, self).__init__(*args, **kwargs)
        
    created = models.DateTimeField(
        auto_now_add=True,
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    task_result = models.ForeignKey(
        TaskResult,
        null=True
    )
    task_id = models.CharField(max_length=128, unique=True)
    resource_url = models.URLField()

    def fetch_task_result(self):
        task_result = TaskResult.objects.get_task(self.task_id)
        if task_result.status != PENDING:
            self.task_result = task_result
            self.save()

    @property
    def status(self):
        if not self.task_result:
            self.fetch_task_result()
        if self.task_result:
            return self.task_result.status
        return PENDING

    @property
    def result(self):
        # if not self.task_result:
        #     self.fetch_task_result()
        if self.task_result:
            return self.task_result.result

    def _get_keywords_result(self, result_type=None):
        if self.task_result and self.status == SUCCESS:
            keywords_result = literal_eval(self.result).get(result_type)
            return keywords_result

    @property
    def keywords_in_content(self):
        return self._get_keywords_result(result_type='in_content')

    @property
    def keywords_not_in_content(self):
        return self._get_keywords_result(result_type='not_in_content')

    @property
    def error_message(self):
        if self.task_result and self.task_result.status != SUCCESS:
            error_message = literal_eval(self.task_result.result)
            return error_message.get('exc_message')

    def save(self, *args, **kwargs):
        if not self.task_id:
            # to znaczy ze nie ma jeszcze powiazania z zadaniem celery
            # i nalezy je stworzyc wtedy i tylko wtedy dostepne sa self.keywords
            keywords_split = filter(None, re.split("[, ]+", self.keywords))
            async_result = process_resource.delay(self.resource_url, keywords_split)
            self.task_id = async_result.task_id
        super(TaskToSession, self).save(*args, **kwargs)


