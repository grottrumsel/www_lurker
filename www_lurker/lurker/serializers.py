# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers

from www_lurker.lurker.models import TaskToSession

KEYWORDS_RE = r'^[,a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]*$'


class ProcessingSessionSerializer(serializers.Serializer):

    resource_urls = serializers.FileField(write_only=True)
    keywords = serializers.RegexField(
        KEYWORDS_RE,
        write_only=True
    )


TASK_RO_FIELDS = (
    'created',
    'status',
    'error_message',
    'keywords_in_content',
    'keywords_not_in_content',
)


class TaskToSessionSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskToSession
        fields = TASK_RO_FIELDS+('keywords', 'user', 'resource_url')
        read_only_fields = TASK_RO_FIELDS
        extra_kwargs = {
            'keywords': {'write_only': True},
            'user': {'write_only': True}
        }

    keywords = serializers.RegexField(
        KEYWORDS_RE,
        write_only=True
    )
    created = serializers.DateTimeField(
        format='%Y-%m-%d %H:%M:%S',
        required=False
    )
