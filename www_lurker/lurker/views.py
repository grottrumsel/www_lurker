# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.views import APIView

from .models import TaskToSession
from .serializers import ProcessingSessionSerializer, TaskToSessionSerializer


class ProcessingSessionAPIView(APIView):
    serializer_class = ProcessingSessionSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        resource_urls = [line.strip().decode('utf8') for line in validated_data['resource_urls']]

        # tworze serializery i inicjuje je danymi
        task_serializers = []
        for resource_url in resource_urls:
            new_task_data = {
                'user': request.user.id,
                'resource_url': resource_url,
                'keywords': validated_data['keywords']
            }
            task_serializers.append(TaskToSessionSerializer(data=new_task_data))

        # sprawdzam czy sa w nich poprawne dane
        invalid_task_serializers = []
        for task_serializer in task_serializers:
            if not task_serializer.is_valid():
                invalid_task_serializers.append(task_serializer)

        # jesli sa jakies niepoprawne to listuje ich bledy
        if invalid_task_serializers:
            response_data = []
            for task_serializer in invalid_task_serializers:
                response_data.append({
                    'resource_url': task_serializer.data['resource_url'],
                    'errors': task_serializer.errors
                })
            return Response(data=response_data, status=status.HTTP_400_BAD_REQUEST)
        else:
            # jesli nie ma zadnego serializera z bledem, zapisuje je
            # tym samym przekazujac zadania do realizacji
            [task_serializer.save() for task_serializer in task_serializers]
            response_data = {}
            return Response(data=response_data, status=status.HTTP_201_CREATED)


class TaskToSessionAPIView(generics.ListAPIView):

    serializer_class = TaskToSessionSerializer

    def get_queryset(self):
        return TaskToSession.objects.filter(user=self.request.user).order_by('-created')
