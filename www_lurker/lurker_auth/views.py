# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import generics
from rest_framework import permissions

from django.contrib.auth import get_user_model

from .serializers import UserSerializer

UserModel = get_user_model()


class UserRegistrationAPIView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny, )
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer