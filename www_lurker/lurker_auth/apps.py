from __future__ import unicode_literals

from django.apps import AppConfig


class LurkerAuthConfig(AppConfig):
    name = 'lurker_auth'
